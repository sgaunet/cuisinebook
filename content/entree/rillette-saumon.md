---
title: "Rillette de saumon"
date: "2024-06-22"
tags: [entrée]
featured_image: ""
description: "Rillette de saumon"
rating: 5
comment: false
website: ""
---

Recette pour 4/6 pers :

* 300g de saumon frais
* 150g de saumon fumé
* 1 jaune d'oeuf
* sel
* poivre
* 100g de beurre 
* Optionnel: +ou- 1 c à soupe d'oeuf de saumon

* Pocher le saumon frais 3/4 mn dans l'eau frémissante salée, l'égoutter, le faire refroidir, l'émietter.
* Couper le saumon fumé en fines lanières, le hacher grossièrement au couteau.
* Mélanger les deux saumons, écraser à la fourchette, ajouter le jaune d'oeuf et le beurre en pommade, saler et poivrer normalement (attention le saumon fumé est souvent salé).
* Bien malaxer à la fourchette, ajouter les oeufs de saumon.
* Réserver au frais au moins une heure.
