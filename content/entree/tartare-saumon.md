---
title: "Tartare au saumon"
date: "2024-06-22"
tags: [entrée]
featured_image: ""
description: "Entrée plutôt classe pour repas de fête"
rating: 5
comment: false
website: ""
---

* Temps de préparation : 40 minutes 
* Temps de cuisson : 20 minutes

Ingrédients (pour 6 personnes) :

- 300 g de lentilles vertes du Berry 
- 400 g de saumon frais
- 100 g de saumon fumé
- 2 échalotes hachées fin
- 5 cornichons
- 2 cuillers à soupe de câpres
- 1 citron
- 1 orange
- 15 cl d'huile d'olive
- piment en poudre
- 1 oignon
- 1 clou de girofle
- thym
- laurier
- 4 cuillers à soupe d’huile de noix
- 2 cuillers à soupe de vinaigre de xéres 
- 1 cuiller à soupe de moutarde
- sel
- poivre blanc
- 1 petit bouquet d’aneth

Préparation de la recette :
                     
* Préparer la vinaigrette des lentilles en faisant monter la moutarde avec l’huile de noix et finir en ajoutant le vinaigre de xérès, le sel et le poivre.
* Laver les lentilles.
* Les mettre dans une casserole. Ajouter 2 fois leur volume d’eau froide. Ajouter le thym, le laurier et l’oignon piqué du clou de girofle. Porter à ébullition. Faire cuire à feu doux 20 mn. Les lentilles doivent être cuites mais fermes.
* Enlever l’oignon et les aromates.
* Egoutter les lentilles. Quand elles sont encore chaudes, les arroser de la vinaigrette à la moutarde. Réserver mais pas au frigo.
* Prélever le zeste du citron et de l’orange à l’économe. Les couper en une fine julienne puis en cubes minuscules. Les faire blanchir séparément dans deux casseroles d’eau bouillante pendant quelques minutes. Les égoutter et réserver.
* Couper les saumons en tout petits cubes. Hacher l’aneth, les cornichons, les échalotes, les câpres. Mélanger dans un saladier les saumons, le hachis, les zestes.
* Arroser avec l’huile d’olive et le jus du citron. Ajouter le piment, du sel et du poivre. Réserver au frais.
* Au moment de servir, presser l’orange. Filtrer et réserver le jus.
* Dresser à l’assiette la salade de lentilles. Ajouter dessus le tartare de saumon. Arroser de jus d’orange. Décorer de brins d’aneth.
