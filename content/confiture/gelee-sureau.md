---
title: "Gelée de sureau"
date: "2024-06-24"
tags: [confiture]
featured_image: ""
description: "Gelée de sureau"
rating: 5
comment: false
website: ""
---

* 30 fleurs
* 75cl jus de pomme
* 75 cl eau
* 1 citron
* 2kg de sucre à confiture

Journée 1:

* nettoyer les fleurs
* mettre les fleurs dans un saladier avec jus et eau et laisser reposer 24h

Jour 2:

* Filtrer les fleurs + jus
* Mettre dans une casserole
* Ajouter le sucre
* Porter à ebullition
* Laisser cuire à petits bouillons pendant 4 min
* Ecumer au cours de la cuisson

Peut se conserver 1 an