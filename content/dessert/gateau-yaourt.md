---
title: "Gateau yahourt"
date: "2023-03-05"
tags: [dessert,gateau]
featured_image: ""
description: "un classique"
rating: 4
comment: false
website: ""
---

Ingrédients:

* 200 gr de farine
* 3 oeufs
* 10 cas huile
* 100 gr sucre
* 1 sachet de levure chimique
* 1 yaourt
* 2 grosses pommes
* +/- 1 cac de rhum

Instructions:

* séparer jaune/blancs
* Battre les blancs
* Mélanger les jaunes + sucre / huile / yaourt + farine + leverue + rhum
* Incorporer les blancs
* Ajouter les dés de pommes
* 45 min à 180° (couvrir les 10 denrnière sminutes)
