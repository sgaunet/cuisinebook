---
title: "tarte au fromage blanc"
date: "2023-03-25"
tags: [dessert,tarte]
featured_image: ""
description: "Tarte au fromage blanc"
rating: 5
comment: false
website: ""
---

* 3 oeufs
* 350gr de fromage blanc
* 100gr de sucre
* 50gr de farine

* Préparer une pâte sablée ou brisée 

* Monter les blancs en neige dans un cul de poule
* Mélanger le reste des ingrédients dans un autre récipient
* Mélanger les 2 préparations à l'aide d'une spatule, attention, mélanger délicatement pour ne pas trop casser les blancs en neige
* Verser la préparation sur votre pâte
* Mettre au four à 180° et surveiller la cuisson (environ 20min)
