---
title: "Cheesecake basque"
date: "2024-04-27"
tags: [dessert,gâteau]
featured_image: ""
description: "Cheese cacke bask"
rating: 5
comment: false
website: ""
---

Ingrédients:

* 600 gr philadelphia
* 400 gr de saint moret
* 1 gousse de vanille
* 6 oeufs
* 220 gr de sucre
* 30gr de farine
* 30 gr creme fraiche entière épaisse (330cl)

Matériel:

* Papier sulfurisé
* moule 22cm

Temps:

* Préparation: 15 min
* Cuisson: 1 heure

Recette:

* Battre les oeufs et le sucre
* Ajouter fromage frais et battre pour que ce soit bien lisse
* Ajouter farine, crême liquide et vanille puis mélanger encore
* Au four à 180° 30 min ? Faut que ça tremblotte encore, à peine cuit au dessus
* Laisser refroidir 1 heure et laisser au moins 5h au réfrigérateur
