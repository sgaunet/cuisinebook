---
title: "cookies"
date: "2023-03-05"
tags: [dessert,biscuit]
featured_image: ""
description: "cookies"
rating: 5
comment: false
website: ""
---

* 1 oeuf
* 85gr de sucre
* 85 gr de beurre (doux ou demi-sel)
* 150 gr de farine
* 100gr pépites de chocolat
* 1 cac de levure chimique
* 1/2 cac de sel (si beurre doux)

Recette:

* Lisse ramollier le beurre à température ambiante (ou 40s 600W au micro onde)
* Mélanger le beurre avec le sucre
* Ajouter la farine (bien mélanger)
* Ajouter l'oeuf
* Ajouter la levure chimique
* Ajouter les pépites de chocolat (ou autre, raisins sec, cacahuètes, noix...)

Cuisson:

* Allumer le four thermostat 6 - 180°
* former des boules à applatir sur une plaque recouverte de papier sulfurisé
* enfourner pour 8-12 min

Le contour des cookies doit être légèrement bruni.

Bonne dégustation