---
title: "crêpes"
date: "2023-03-05"
tags: [dessert,crepes]
featured_image: ""
description: "Crêpes"
rating: 5
comment: false
website: ""
---

* 4 oeufs (ou 3 gros)
* 250gr de farine
* 600ml de lait
* 40gr de beurre

* dans un cul de poule, mettre tous les ingrédients, farine en dernier
* Mixer (oui oui mais comme ça, ça va vite)

* Mettre au frais, c'est meilleur si la pâte repose 2/3h

Normalement, pas besoin de graisser la poêle, le beurre dans la pâte suffit.