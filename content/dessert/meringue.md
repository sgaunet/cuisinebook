---
title: "Meringue"
date: "2024-01-05"
tags: [dessert,meringue]
featured_image: ""
description: "Meringue"
rating: 5
comment: false
website: ""
---

Ingrédients:

* 4 blancs d'oeuf
* 250gr de sucre
* 1 pincée de sel

Recette:

* Préchauffer le four à 140°
* Battre les blancs d'oeuf + pincée de sel
* Une fois les oeufs montés en neige, ajouter le sucre petit à petit
* Utiliser une poche à douille pour former les meringue sur une plaque munie d'un papier sulfurisé
* Mettre au four
* Baisser la température à 100° et laisser cuire 30 min

PS: La recette notée ci-dessus est modifiée par rapport à l'originelle et n'a pas été testée mais ça devrait le faire...
