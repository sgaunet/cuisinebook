---
title: "Cheesecake basque v2"
date: "2024-06-22"
tags: [dessert,gâteau]
featured_image: ""
description: "Cheesecake basque v2"
rating: 5
comment: false
website: ""
---

Ingrédients:

* 300gr de philadelphia
* 1 gousse de vanille
* 2 oeufs
* 90 gr de sucre
* 165gr de crème liquide
* 1 gr d'amidon

Matériel:

* Papier sulfurisé
* moule 18cm

Temps:

* Préparation: 15 min
* Cuisson: 22min

Recette:

* Battre les oeufs et le sucre
* Ajouter fromage frais et battre pour que ce soit bien lisse
* Ajouter amidon, crême liquide et vanille puis mélanger encore
* Au four à 230° pour 22min
* Laisser refroidir 1 heure et laisser au moins 5h au réfrigérateur
