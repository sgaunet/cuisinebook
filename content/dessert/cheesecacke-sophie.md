---
title: "Cheese cake (variante sophie)"
date: "2024-06-25"
tags: [dessert]
featured_image: ""
description: "Variante de cheesecake, plutôt gateau au fromage blanc"
rating: 4
comment: false
website: ""
---

Fond de tarte:

* 150gr de farine
* 100gr de beurre
* 75gr de sucre
* 1 jaune d'oeuf
* 1 cac de levure chimique

Garniture:

* 750gr de fromage blanc 20%
* 250gr mascarpone
* 100gr de sucre
* 4 jaunes d'oeufs
* 4 blancs d'oeuf en neige
* 1 paquet de sucre vanillé
* 4 cas de farine

Instructions:

* Fond de tarte: travailler comme la pâte brisée. Tapisser le fond du moule
* Garniture: Mélanger les jaunes avec le sucre + farine + fromage blanc. Monter les blancs en neige et ajouter au reste.
* Cuisson: 55' à 180°
