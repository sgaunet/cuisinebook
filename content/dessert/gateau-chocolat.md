---
title: "Gâteau au chocolat"
date: "2023-01-05"
tags: [dessert,gâteau,chocolat]
featured_image: ""
description: "Gâteau au chocolat fondant"
rating: 5
comment: false
website: ""
---

Ingrédients:

* 2 CAS de farine
* 200gr de chocolat
* 4 oeufs
* 200gr de sucre
* 200gr de beurre
* 1 pincée de sel

Recette:

* Faire fondre le chocolat avec le beurre
* Battre les oeufs et le sucre
* Ajouter le chocolat/beurre fondu
* Ajouter 2 cas de farine
* Verser la pâte dans un plat beurrée
* Mettre au four à 190° 20 min
