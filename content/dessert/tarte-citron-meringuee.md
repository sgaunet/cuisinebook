---
title: "tarte citron meringuée"
date: "2024-09-08"
tags: [dessert,tarte]
featured_image: ""
description: "Tarte citron meringuée"
rating: 5
comment: false
website: ""
---

## Pâte sablée:

* 200 gr de farine T45
* 90gr de sucre glace
* 25gr de poudre d'amande
* 1 pincée de fleur de sel
* 100gr beurre demi sel
* 1 oeuf entier

## Crême citron:

* 120gr sucre
* 90gr de jaune d'oeuf (5 jaune d'oeufs)
* 40gr fleur de Maïs (Maizena)
* 200gr de jeus de citron (4 citrons)
* 100gr d'eau
* 50gr de beurre demi sel

## Meringue:

* 4 blanc d'oeufs (140 gr)
* 125gr de sucre
* 125gr sucre glace


Pâte sablée:

* Tamiser farine + sucre glace + poudre d'amande
* ajouter beurre coupé en morceaux et mélanger du bout des doitgs
* Mettre le sablage en fontaine et ajouter sel + oeuf légèrement battu à la fourchette
* Ne pas trop travailler la pâte
* filmer et réserver une heure
* Cuire à blanc 15 min à 180

Crême citron:

* Jus de citron + eau à bouillir
* blanchir les jaunes et le sucre
* Tamiser maizena et ajouter au jaune + sucre
* verser la preparation dans jus et eau hors du feu
* reporter sur le feu et remuer au fouet comme une crême patissiere
* laisser refroidir

Meringue:

* Monter les blancs en neige
* Ajouter le sucre, mélanger rapidement (ça doit briller)
* Ajouter le sucre glace, la consistance doit être lisse, homogène et brillante

Final:

* Préchauffer le four à 90
* Garnir la pâte sablée avec la crême
* Ajouter la meringue dessus (avec une poche à douille)
* Cuire 1h à 90°

Bonne dégustation
