---
title: "Garbure béarnaise"
date: "2023-02-05"
tags: [soupe,garbure,traditionnel]
featured_image: ""
description: "Recette garbure"
rating: 5
comment: false
website: ""
---

* 1 talon de jambon
* 9 grains de poivre
* 1 piment d'espelette
* 1 oignon piqué de 4 clous de girofle
* 1 branche de celeri
* 1 branche de thym
* 1 feuille de leurier
* 1 chou vert
* 6 carottes
* 4 navets
* 2 poireaux
* 6 pommes de terre
* 250 gr de haricots tarbais
* 8 morceaux de confit de canard

**Si les haricots sont sec, déposez les dans un saladier et recouvrer d'eau au moins 12 heures.** A faire la veille :D

Dans un grand faitout, mettre:

* talon de jambon
* oignon piqué des clous de girofle
* laurier
* thym
* poivre
* pimen d'espelette
* céléri branche

Recouvrer d'eau, environ 5 litres et laisser mijoter 1h30.


Pendant ce temps, laver/éplucher/couper les légumes en morceaux grossier sauf les poireaux en petits morceaux.

A la poêle, faire dorer les confits de canard. Une fois dorés, réserver les.
Faire revenir les légumes (poireaux, carottes, navets) dans le gras de canard de la poê qui vient d'être utilisée.


Au bout d'1h30 de cuisson, rajouter:

* les légumes (poireaux, carottes, navets)
* les haricots blancs


Ensuite:

* Laisser mijoter doucement 2 heures et ajouter le chou.
* Laisser mijoter 30 min et ajouter les pommes de terre
* Laisser mijoter 30 min et ajouter les confits
* Laisser cuire 15/20 minutes

C'est prêt!
