---
title: "Soupe Miso"
date: "2024-09-08"
tags: [soupe]
featured_image: ""
description: "Soupe miso"
rating: 5
comment: false
website: ""
---

Ingrédients:

* 1/2 oignon
* 20gr de carotte
* tofu
* algues wakamé
* huile sésame
* 60gr de miso
* 1/2 poireau

Recette:

* Couper l'oignon et les carottes en fin morceaux.
* Faites revenir 20 morceaux de tofu dans un peu d'huile
* laver et couper 3gr de wakamé déshydraté
* Dans une casserole, faites revenir l'oignon avec de l'huile de sésame
  * ajouter le tofu
  * ajouter la carotte
  * couvrer
* la cuisson terminée
  * ajouter 3 tasses d'eau
  * cuire à feu vif sans couvercle
* à ébullition, baisser le feu
* ajouter le wakame
* cuire 2 min en couvrant
* retirer du feu et ajouter les 60gr de miso
* ajouter le poireau finement coupé en rondelle

Bonne dégustation
