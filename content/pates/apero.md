---
title: "Gâteau apéro"
date: "2023-06-20"
tags: [gateaux,apero]
featured_image: ""
description: "Recette de gâteau d'apéro à retravailler"
rating: 3
comment: false
website: ""
---

* 90 gr de farine
* 2 cas huile d'olive
* pincée de sel
* 1 oeuf
* 1 cc beurre de cacahuète (remplacer par du beurre si indispo)


* Préchauffer le four à 180°
* Mélanger tous les ingrédients
* Faire des petits biscuits à la main
* Ajouter au choix
  * noix
  * graines de pavot
  * gingembre
  * curry
  * graînes de sésames
  * ...
* Mettre au four environ 13min (à surveiller)

