---
title: "Gougères"
date: "2024-01-17"
tags: [pate,chou,choux,gougères]
featured_image: ""
description: "Rectte des gougères"
rating: 5
comment: false
website: ""
---

* Préparation: 15 min
* Cuisson: 30min à 190°

Les gougères peuvent se congeler (à réchauffer au moment de servir).

Ingrédients pour 15 gougères:

* 250 ml d'eau
* 1/2 cac de sel + poivre
* 75gr de beurre
* 150gr de farine
* 100gr comté râpé
* 4 oeufs 

Recette:

* Mettre dans une casserolle:
  * eau
  * sel
  * poivre
  * beurre coupé en morceaux
* Faire bouillir
* Dès l'ébullition
  * retirer du feu et mettre toute la farine d'un coup
  * remuer energiquement à la spatule jusqu'à qu'il se forme une boule se détachant de la casserole
* Casser le 1ier oeuf et l'ajouter à la pâte
* Mélanger parfaitement
* Répéter l'opération avec les autres oeufs
* Ajouter le fromage râpé
* Former des petits choux avec 2 cac sur une plaque allant au four
* enfourner 30min à 190°, chaleur tournante, plaque au milieu du four
