---
title: "Pâte pizza"
date: "2023-02-05"
tags: [pate,pizza]
featured_image: ""
description: "Pâte pizza"
rating: 5
comment: false
website: ""
---

* 500 gr de farine (type 65/très fine)
* 300ml d'eau tiède
* 15gr de levure fraîche
* 3 cuillerées à café rases de sel fin
* 1 cuillerée à café de sucre
* 3 cuillerées à soupe d'huile d'olive


* Mettre l'eau dans le bol du robot
* Ajouter la levure et la délayer
* Ajouter le sucre
* Laisser reposer 15/20 min

* Ajouter la faine
* le sel
* l'huile d'olive

* Pétrir au robot pendant 10 min.
* Sortir le bol, mettre un torchon humide dessus et déposer le bol prêt d'un radiateur

Il est possible de mettre aussi le bol dans le four (à 50°), stopper le four dès enfournement.
