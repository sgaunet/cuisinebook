---
title: "Pain sans pétrissage"
date: "2023-03-25"
tags: [pate,pain]
featured_image: ""
description: "Pain sans pétrissage"
rating: 5
comment: false
website: ""
---

* 500 gr de farine
* 1 cac de sel
* 1 cac de sucre
* 8gr de levure fraiche
* 370 gr d'eau

* Mélanger tous les ingrédients SAUF l'eau
* Mélanger avec l'eau, grossièrement
* Couvrer le mélange d'un film plastique
* Laisser reposer entre 8h et 12h

* Fariner le plan de travail
* Mettre en forme
* Mettre le plan à l'envers dessus pour couvrir en attendant

* Préchauffer le four à 200°
* Mettre la marmitte dans laquelle le pain va cuire - 15 min
* Sortie le plat, y placer la pâte + le couvercle
* Laisser cuire 30 min
* Retirer le couvercle et laisser cuire 15 min supplémentaire
* Laisser refroidir

